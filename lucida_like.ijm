// 1) Open OIB/OIR file, pairwise stitch with sub pixel accuracy, max Intensity
// 2) Save this as a tiff without changing anything else.
// 3) Segment neuron in 3D Slicer and save the segementation as an nrrd file. Unselect crop in save options.
// 4) Open the tif/nrrd file.
// 5) Run this script

image_id = getImageID();

// All confocal scans were done with the same setting.
// Voxel dimension: x, y, z: 2.4859, 2.4859, 1
Stack.setXUnit("um"); Stack.setYUnit("um"); Stack.setZUnit("um");
run("Properties...", "pixel_width=2.4859 pixel_height=2.4859 voxel_depth=1");

// NRRD files have pixel values 0 or 1. 1 is the segmented area, 0 everywhere else.
if (matches(getInfo("image.filename"), ".*nrrd")) {
  setMinAndMax(0, 1);
  run("Apply LUT", "stack");
}

// Do a 3D projection between 0-20 degree at 1 degree incremement for a sanity check of stitching.
run("3D Project...", "projection=[Brightest Point] axis=Y-Axis slice=1 initial=0 total=20 rotation=1 lower=1 upper=255 opacity=0 surface=100 interior=50 interpolate");

// The actual steps to make camera lucida like images of tissue and segmented neuron
selectImage(image_id);
run("Invert", "stack");
run("Z Project...", "projection=[Min Intensity]");
run("8-bit");

//saveAs("PNG");