# Lucida like

Create camera-lucida like images from confocal images and tissue segmented using 3D Slicer

See "how_to_lucida_like.mp4" for usage.
